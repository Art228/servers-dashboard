import React from "react";
import { IconButton, MenuItem, Menu } from "@material-ui/core";
import MoreHoriz from "@material-ui/icons/MoreHoriz";

const MenuButton = ({ id, status, switchStatus, reboot }) => {
  const [anchorEl, setAnchorEl] = React.useState(null);
  const open = Boolean(anchorEl);

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleSwitch = () => {
    switchStatus(id);
    handleClose();
  };

  const handleReboot = () => {
    reboot(id);
    handleClose();
  };

  return (
    <div>
      <IconButton
        aria-label="account of current user"
        aria-controls="menu-appbar"
        aria-haspopup="true"
        onClick={handleMenu}
        color="inherit"
      >
        <MoreHoriz />
      </IconButton>
      <Menu
        id="menu-appbar"
        anchorEl={anchorEl}
        anchorOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        keepMounted
        transformOrigin={{
          vertical: "top",
          horizontal: "right",
        }}
        open={open}
        onClose={handleClose}
      >
        <MenuItem onClick={handleSwitch}>
          {status === "ONLINE" ? "Turn off" : "Turn on"}
        </MenuItem>
        <MenuItem onClick={handleReboot}>Reboot</MenuItem>
      </Menu>
    </div>
  );
};

export default MenuButton;
