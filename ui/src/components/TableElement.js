import React, { useState } from "react";
import TableCell from "@material-ui/core/TableCell";
import MenuButton from "./MenuButton";

import OnlineLabel from "./OnlineLabel";
import OfflineLabel from "./OfflineLabel";
const TableElement = ({ row }) => {
  const [status, setStatus] = useState(row.status);

  const switchStatus = async (id) => {
    try {
      const s = status === "ONLINE" ? "off" : "on";

      const response = await fetch(`/servers/${id}/${s}`, {
        method: "PUT",
        headers: { "Content-Type": "application/json" },
      });
      const res = await response.json();
      console.log(res);
      setStatus(res.status);
    } catch (error) {
      console.error(error.message);
    }
  };

  const reboot = async (id) => {
    try {
      if (status === "ONLINE") {
        const response = await fetch(`/servers/${id}/reboot`, {
          method: "PUT",
          headers: { "Content-Type": "application/json" },
        });
        const res = await response.json();
        setStatus(res.status);
        ping(id);
      }
    } catch (error) {
      console.error(error.message);
    }
  };

  const ping = async (id) => {
    try {
      const response = await fetch(`/servers/${id}`);
      const res = await response.json();

      if (res.status === "ONLINE") {
        setStatus(res.status);
        return;
      } else if (res.status === "REBOOTING") {
        setTimeout(() => ping(id), 1000);
      }
    } catch (err) {
      console.log(err);
    }
  };

  return (
    <>
      <TableCell component="th" scope="row">
        {row.name}
      </TableCell>
      <TableCell>
        {status === "ONLINE" ? (
          <OnlineLabel />
        ) : status === "OFFLINE" ? (
          <OfflineLabel />
        ) : (
          "REBOOTING..."
        )}
      </TableCell>
      <TableCell align="right">
        <MenuButton
          id={row.id}
          status={status}
          switchStatus={switchStatus}
          reboot={reboot}
        />
      </TableCell>
    </>
  );
};

export default TableElement;
