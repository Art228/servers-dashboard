import React from "react";
import { Grid, makeStyles } from "@material-ui/core";
import OfflineBolt from "@material-ui/icons/OfflineBolt";

const useStyles = makeStyles({
  container: {
    display: "flex",
    alignItems: "center",
  },

  icon: {
    color: "red",
  },

  txt: {
    marginLeft: 10,
    color: "red",
  },
});

const OnlineLabel = () => {
  const classes = useStyles();

  return (
    <Grid container spacing={0} className={classes.container}>
      <Grid item>
        <OfflineBolt fontSize="small" className={classes.icon} />
      </Grid>
      <Grid item>
        <p className={classes.txt}>OFFLINE</p>
      </Grid>
    </Grid>
  );
};

export default OnlineLabel;
