import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import { Table } from "@material-ui/core";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Container from "@material-ui/core/Container";
import IconButton from "@material-ui/core/IconButton";
import InputAdornment from "@material-ui/core/InputAdornment";
import SearchIcon from "@material-ui/icons/Search";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";

import TableElement from "./TableElement";

const useStyles = makeStyles({
  container: {
    paddingTop: 30,
    paddingBottom: 30,
  },

  table: {
    minWidth: 400,
  },

  textField: {
    marginTop: 50,
  },
});

const Servers = () => {
  const classes = useStyles();
  const [searchTerm, setSearchTerm] = useState("");
  const [searchResults, setSearchResults] = useState([]);

  const getServers = async () => {
    try {
      const response = await fetch("/servers");
      const jsonData = await response.json();
      setSearchResults(jsonData);
    } catch (error) {
      console.error(error.message);
    }
  };

  useEffect(() => {
    getServers();
  }, []);

  const results = !searchTerm
    ? searchResults
    : searchResults.filter((i) =>
        i.name.toLowerCase().includes(searchTerm.toLowerCase())
      );

  const handleChange = (event) => {
    setSearchTerm(event.target.value);
  };

  return (
    <Container maxWidth="md" className={classes.container}>
      <Grid container spacing={2}>
        <Grid item xs={8}>
          <h2 align="left">Servers </h2>
          <h4 align="left">Number of elements: {results.length} </h4>
        </Grid>
        <Grid item xs={4}>
          <TextField
            className={classes.textField}
            label="Search"
            value={searchTerm}
            onChange={handleChange}
            InputProps={{
              endAdornment: (
                <InputAdornment>
                  <IconButton>
                    <SearchIcon />
                  </IconButton>
                </InputAdornment>
              ),
            }}
          />
        </Grid>
      </Grid>
      <TableContainer component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Name</TableCell>
              <TableCell>Status</TableCell>
              <TableCell align="right">&nbsp;</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {results.map((row) => (
              <TableRow key={row.id}>
                <TableElement row={row} />
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
    </Container>
  );
};

export default Servers;
