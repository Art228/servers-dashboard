import React, { Component } from "react";
import { createMuiTheme, ThemeProvider } from "@material-ui/core/styles";
import "./App.css";
import AppBar from "./components/AppBar";
import Servers from "./components/Servers";

const theme = createMuiTheme({
  palette: {
    primary: {
      main: "#2E3B55",
    },
  },
});

class App extends Component {
  render() {
    return (
      <ThemeProvider theme={theme}>
        <div className="App">
          <AppBar />
          <Servers />
        </div>
      </ThemeProvider>
    );
  }
}

export default App;
