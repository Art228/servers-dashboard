import React from "react";
import { Grid, makeStyles } from "@material-ui/core";
import RadioButtonUnchecked from "@material-ui/icons/RadioButtonUnchecked";

const useStyles = makeStyles({
  container: {
    display: "flex",
    alignItems: "center",
  },

  icon: {
    color: "green",
  },

  txt: {
    marginLeft: 10,
    color: "green",
  },
});

const OnlineLabel = () => {
  const classes = useStyles();

  return (
    <Grid container spacing={0} className={classes.container}>
      <Grid item>
        <RadioButtonUnchecked fontSize="small" className={classes.icon} />
      </Grid>
      <Grid item>
        <p className={classes.txt}>ONLINE</p>
      </Grid>
    </Grid>
  );
};

export default OnlineLabel;
